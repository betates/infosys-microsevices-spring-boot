# learning
### Screenshot API Test
GET : localhost:8080/api/getpersonname<br>
![Screenshot1](screenshot/task_1.png)<br><br>
GET : localhost:8080/api/getpersonnamev2<br>
![Screenshot2](screenshot/task_2.png)<br><br>
POST : localhost:8080/api/getpersonnamev3<br>
![Screenshot3](screenshot/task_2.1.png)<br><br>
POST : localhost:8080/api/getperson<br>
![Screenshot4](screenshot/task_3.png)<br><br>
POST : localhost:8080/api/register (success)<br>
![Screenshot5](screenshot/task_4.png)<br><br>
POST : localhost:8080/api/register (failed)<br>
![Screenshot6](screenshot/task_4.1.png)<br><br>
POST : localhost:8080/api/login (success)<br>
![Screenshot7](screenshot/task_5.png)<br><br>
POST : localhost:8080/api/login (failed)<br>
![Screenshot8](screenshot/task_5.1.png)